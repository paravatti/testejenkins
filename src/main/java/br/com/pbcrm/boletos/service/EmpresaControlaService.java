package br.com.pbcrm.boletos.service;

import br.com.pbcrm.boletos.persistence.EmpresaControlada;
import br.com.pbcrm.boletos.persistence.EmpresaControladaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpresaControlaService {
    @Autowired
    private EmpresaControladaRepository empresaControladaRepository;

    public List<EmpresaControlada> getAll() { return empresaControladaRepository.findAll();}

    public EmpresaControlada create(EmpresaControlada empresaControlada){ return empresaControladaRepository.save(empresaControlada);}


}
