package br.com.pbcrm.boletos.service;

import br.com.pbcrm.boletos.persistence.EmpresaControlada;
import br.com.pbcrm.boletos.persistence.RelacaoBancos;
import br.com.pbcrm.boletos.persistence.RelacaoBancosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelacaoBancosService {
    @Autowired
    RelacaoBancosRepository relacaoBancosRepository;

    public List<RelacaoBancos> getAll() { return relacaoBancosRepository.findAll();}

    public RelacaoBancos create(RelacaoBancos relacaoBancos){ return relacaoBancosRepository.save(relacaoBancos);}

    public void delete(RelacaoBancos relacaoBancos){
        relacaoBancosRepository.deleteById(relacaoBancos.getIdBanco());
    }

    public RelacaoBancos update(RelacaoBancos relacaoBancos){ return relacaoBancosRepository.save(relacaoBancos);}

}
