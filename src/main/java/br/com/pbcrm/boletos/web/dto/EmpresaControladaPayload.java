package br.com.pbcrm.boletos.web.dto;

import br.com.pbcrm.boletos.persistence.EmpresaControlada;

public class EmpresaControladaPayload {

    int IdEmpresa;
    String NomeEmpresa;
    int TipoPessoaEmpresa;
    int CNPJEmpresa;
    int CPFEmpresa;
    String InstrucoesPadraoBoleto;
    double JurosVencimentoBoleto;
    double MoraVencimentoBoleto;
    int PrazoLimitePagamentoBoleto;

    public EmpresaControladaPayload() { }

    public EmpresaControladaPayload(EmpresaControlada empresaControlada) {
        IdEmpresa = empresaControlada.getIdEmpresaControlada();
        NomeEmpresa = empresaControlada.getNomeEmpresaControlada();
        TipoPessoaEmpresa = empresaControlada.getTipoPessoaEmpresaControlada();
        CNPJEmpresa = empresaControlada.getCNPJEmpresaControlada();
        CPFEmpresa = empresaControlada.getCPFEmpresaControlada();
        InstrucoesPadraoBoleto = empresaControlada.getInstrucoesPadraoBoleto();
        JurosVencimentoBoleto = empresaControlada.getJurosVencimentoBoleto();
        MoraVencimentoBoleto = empresaControlada.getMoraVencimentoBoleto();
        PrazoLimitePagamentoBoleto = empresaControlada.getPrazoLimitePagamentoBoleto();
    }

    public int getIdEmpresa() {return IdEmpresa; }

    public void setIdEmpresa(int idEmpresa) {IdEmpresa = idEmpresa; }

    public String getNomeEmpresa() {return NomeEmpresa; }

    public void setNomeEmpresa(String nomeEmpresa) {NomeEmpresa = nomeEmpresa; }

    public int getTipoPessoaEmpresa() {return TipoPessoaEmpresa; }

    public void setTipoPessoaEmpresa(int tipoPessoaEmpresa) {TipoPessoaEmpresa = tipoPessoaEmpresa; }

    public int getCNPJEmpresa() {return CNPJEmpresa; }

    public void setCNPJEmpresa(int CNPJEmpresa) {this.CNPJEmpresa = CNPJEmpresa; }

    public int getCPFEmpresa() {return CPFEmpresa; }

    public void setCPFEmpresa(int CPFEmpresa) {this.CPFEmpresa = CPFEmpresa; }

    public String getInstrucoesPadraoBoleto() {return InstrucoesPadraoBoleto;}

    public void setInstrucoesPadraoBoleto(String instrucoesPadraoBoleto) { InstrucoesPadraoBoleto = instrucoesPadraoBoleto;}

    public double getJurosVencimentoBoleto() {return JurosVencimentoBoleto; }

    public void setJurosVencimentoBoleto(double jurosVencimentoBoleto) {JurosVencimentoBoleto = jurosVencimentoBoleto; }

    public double getMoraVencimentoBoleto() {return MoraVencimentoBoleto; }

    public void setMoraVencimentoBoleto(double moraVencimentoBoleto) {MoraVencimentoBoleto = moraVencimentoBoleto; }

    public int getPrazoLimitePagamentoBoleto() {return PrazoLimitePagamentoBoleto; }

    public void setPrazoLimitePagamentoBoleto(int prazoLimitePagamentoBoleto) {PrazoLimitePagamentoBoleto = prazoLimitePagamentoBoleto; }

    public EmpresaControlada buildEntity(){
        EmpresaControlada empresaControlada = new EmpresaControlada();
        empresaControlada.setIdEmpresaControlada(IdEmpresa);
        empresaControlada.setNomeEmpresaControlada(NomeEmpresa);
        empresaControlada.setCNPJEmpresaControlada(CNPJEmpresa);
        empresaControlada.setCPFEmpresaControlada(CPFEmpresa);
        empresaControlada.setInstrucoesPadraoBoleto(InstrucoesPadraoBoleto);
        empresaControlada.setJurosVencimentoBoleto(JurosVencimentoBoleto);
        empresaControlada.setMoraVencimentoBoleto(MoraVencimentoBoleto);
        empresaControlada.setPrazoLimitePagamentoBoleto(PrazoLimitePagamentoBoleto);
        empresaControlada.setTipoPessoaEmpresaControlada(TipoPessoaEmpresa);

        return empresaControlada;
    }
}
