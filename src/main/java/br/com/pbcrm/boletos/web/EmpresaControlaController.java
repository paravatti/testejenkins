package br.com.pbcrm.boletos.web;

import br.com.pbcrm.boletos.persistence.EmpresaControlada;
import br.com.pbcrm.boletos.service.EmpresaControlaService;
import br.com.pbcrm.boletos.web.dto.EmpresaControladaPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/EmpresaControlada")
public class EmpresaControlaController {
    @Autowired
    private EmpresaControlaService empresaControlaService;

    @GetMapping
    public List<EmpresaControlada> getAll(){ return empresaControlaService.getAll();}

    @PostMapping
    public EmpresaControladaPayload create(@RequestBody EmpresaControladaPayload payload){
             EmpresaControlada empresaControlada = payload.buildEntity();
            return  new EmpresaControladaPayload(empresaControlaService.create(empresaControlada));
    }
}
