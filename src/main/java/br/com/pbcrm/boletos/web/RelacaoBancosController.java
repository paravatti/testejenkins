package br.com.pbcrm.boletos.web;

import br.com.pbcrm.boletos.persistence.EmpresaControlada;
import br.com.pbcrm.boletos.persistence.RelacaoBancos;
import br.com.pbcrm.boletos.service.RelacaoBancosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Bancos")
public class RelacaoBancosController {
    @Autowired
    RelacaoBancosService relacaoBancosService;

    @GetMapping
    public List<RelacaoBancos> getAll(){ return relacaoBancosService.getAll();}

    @PostMapping
    public RelacaoBancos create(@RequestBody RelacaoBancos relacaoBancos){
            return relacaoBancosService.create(relacaoBancos);
    }

    @DeleteMapping
    public List<RelacaoBancos> delete(@RequestBody RelacaoBancos relacaoBancos){
        List<RelacaoBancos> bancosdeletados = new ArrayList<>();

        for (RelacaoBancos bancosnodb : relacaoBancosService.getAll()){
            if (relacaoBancos.getCodigoCompensacaoBanco() == bancosnodb.getCodigoCompensacaoBanco()) {
                bancosdeletados.add(bancosnodb);
                relacaoBancosService.delete(bancosnodb);
            }
        }
        return bancosdeletados;
    }
    @PutMapping("/{bancoId}")
    public RelacaoBancos update(@PathVariable int bancoId, @RequestBody RelacaoBancos relacaoBancos){
        relacaoBancos.setIdBanco(bancoId);
        return relacaoBancosService.update(relacaoBancos);
    }
}
