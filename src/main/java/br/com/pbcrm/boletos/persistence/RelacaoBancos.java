package br.com.pbcrm.boletos.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RelacaoBancos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int IdBanco;
    private int CodigoCompensacaoBanco;
    private String NomeBanco;

    public int getCodigoCompensacaoBanco() {return CodigoCompensacaoBanco;}

    public void setCodigoCompensacaoBanco(int codigoCompensacaoBanco) {
        CodigoCompensacaoBanco = codigoCompensacaoBanco;
    }

    public String getNomeBanco() {return NomeBanco;}

    public void setNomeBanco(String nomeBanco) {NomeBanco = nomeBanco; }

    public int getIdBanco() {return IdBanco; }

    public void setIdBanco(int idBanco) {IdBanco = idBanco;}
}
