package br.com.pbcrm.boletos.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EmpresaControlada {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int IdEmpresaControlada;
    private String NomeEmpresaControlada;
    private int TipoPessoaEmpresaControlada;
    private int CNPJEmpresaControlada;
    private int CPFEmpresaControlada;
    private String InstrucoesPadraoBoleto;
    private double JurosVencimentoBoleto;
    private double MoraVencimentoBoleto;
    private int PrazoLimitePagamentoBoleto;

    public int getIdEmpresaControlada() {return IdEmpresaControlada; }

    public void setIdEmpresaControlada(int idEmpresaControlada) {IdEmpresaControlada = idEmpresaControlada;    }

    public String getNomeEmpresaControlada() {return NomeEmpresaControlada;}

    public void setNomeEmpresaControlada(String nomeEmpresaControlada) {
        NomeEmpresaControlada = nomeEmpresaControlada;
    }

    public int getTipoPessoaEmpresaControlada() {
        return TipoPessoaEmpresaControlada;
    }

    public void setTipoPessoaEmpresaControlada(int tipoPessoaEmpresaControlada) {
        TipoPessoaEmpresaControlada = tipoPessoaEmpresaControlada;
    }

    public int getCNPJEmpresaControlada() {
        return CNPJEmpresaControlada;
    }

    public void setCNPJEmpresaControlada(int CNPJEmpresaControlada) {
        this.CNPJEmpresaControlada = CNPJEmpresaControlada;
    }

    public int getCPFEmpresaControlada() {
        return CPFEmpresaControlada;
    }

    public void setCPFEmpresaControlada(int CPFEmpresaControlada) {
        this.CPFEmpresaControlada = CPFEmpresaControlada;
    }

    public String getInstrucoesPadraoBoleto() {
        return InstrucoesPadraoBoleto;
    }

    public void setInstrucoesPadraoBoleto(String instrucoesPadraoBoleto) {
        InstrucoesPadraoBoleto = instrucoesPadraoBoleto;
    }

    public double getJurosVencimentoBoleto() {
        return JurosVencimentoBoleto;
    }

    public void setJurosVencimentoBoleto(double jurosVencimentoBoleto) {
        JurosVencimentoBoleto = jurosVencimentoBoleto;
    }

    public double getMoraVencimentoBoleto() {
        return MoraVencimentoBoleto;
    }

    public void setMoraVencimentoBoleto(double moraVencimentoBoleto) {
        MoraVencimentoBoleto = moraVencimentoBoleto;
    }

    public int getPrazoLimitePagamentoBoleto() {
        return PrazoLimitePagamentoBoleto;
    }

    public void setPrazoLimitePagamentoBoleto(int prazoLimitePagamentoBoleto) {
        PrazoLimitePagamentoBoleto = prazoLimitePagamentoBoleto;
    }
}
