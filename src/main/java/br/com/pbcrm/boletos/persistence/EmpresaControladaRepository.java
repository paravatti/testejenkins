package br.com.pbcrm.boletos.persistence;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface EmpresaControladaRepository extends CrudRepository<EmpresaControlada, Integer> {
    List<EmpresaControlada> findAll();

}
