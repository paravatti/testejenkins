package br.com.pbcrm.boletos.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RelacaoBancosRepository extends CrudRepository<RelacaoBancos, Integer> {
    List<RelacaoBancos> findAll();
}

