projeto de boleto
o Body da mensagem sempre em Json (RAW)

**Empresas Controladas**
    /EmpresasControladas
        Listar
            GET: http://localhost:8080/EmpresaControlada
            
        Incluir uma nova
            Post: http://localhost:8080/EmpresaControlada
            {
                "tipoPessoaEmpresa": 1,
                "jurosVencimentoBoleto": 1.5,
                "moraVencimentoBoleto": 2.0,
                "prazoLimitePagamentoBoleto": 30,
                "instrucoesPadraoBoleto": "instrucoes numero 1",
                "nomeEmpresa": "Empresa 1!",
                "cnpjempresa": 33,
                "cpfempresa": 22
            }

**Relação de Bancos**
        Listar
            Get : http://localhost:8080/Bancos
        Incluir
            Post: http://localhost:8080/Bancos
                {
                    "codigoCompensacaoBanco" : 237,
                    "nomeBanco" : "BANCO ITAÚ S.A. "
                }

        Deletar por Cod Compensação

            DELETE: http://localhost:8080/Bancos
                {
                    "codigoCompensacaoBanco" : 237
                }

        Atualizar todo registro
            PUT: http://localhost:8080/Bancos/{idbanco
                {
                  "codigoCompensacaoBanco" : 237,
                  "nomeBanco" : "BANCO ITAÚ S.A. "
                }


